# QnMod (Queue and Module) #

### What is this repository for? ###

#### Quick summary ####

Create your application easily with QnMod.  
All you need is to split your application into modules (network module, logic module, database module, etc...).  
Inspired by [ITHare](http://ithare.com/asynchronous-processing-for-finite-state-machines-actors-from-plain-events-to-futures-with-oo-and-lambda-call-pyramids-in-between/)

#### How it works ####

All modules you create must inherit from BaseModule.  
Internally, BaseModule uses [Facebook folly EventBase class](https://github.com/facebook/folly/tree/master/folly/io/async).  
Inherit your modules from BaseModule. Add them in the ModuleLocator. Start them. And use ModuleLocator's enqueueTaskInModule to talk to each other and start tasks.  
Example : A microservice application with a network module that receives a message from a client, delivers this message to logic module, the logic module processes this message, sends the result to network module and the network module sends it to the client through the network.

### How do I get set up? ###

You must use a C++14 compiler.  
You can use [vcpkg](https://github.com/Microsoft/vcpkg) to get dependencies easily.

#### Dependencies ####

* [folly](https://github.com/facebook/folly)
* [nng](https://github.com/nanomsg/nng)

#### TODO ####

* Unit testing
* Continuous integration
* Generate doxygen documentation
* Wiki with examples