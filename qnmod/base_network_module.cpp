#include "base_network_module.h"
#include "base_network_connection.h"

BaseNetworkModule::BaseNetworkModule() : BaseModule()
{

}

BaseNetworkModule::~BaseNetworkModule()
{
	m_Connections.clear();
}

void BaseNetworkModule::sendMessageTo(uint8_t connectionid, uint8_t routetype, std::shared_ptr<BaseMessage> message)
{
	nng_msg* msg;
	nng_msg_alloc(&msg, 0);

	std::string bytes = m_Connections.at(connectionid)->serializeMessage(message);
	nng_msg_append(msg, bytes.c_str(), bytes.size());
	m_Connections.at(connectionid)->onPreSendMessage(routetype, msg, message);

	nng_aio_set_msg(m_Connections.at(connectionid)->getSendAIO(), msg);
	nng_send_aio(m_Connections.at(connectionid)->getSocket(), m_Connections.at(connectionid)->getSendAIO());
	nng_aio_wait(m_Connections.at(connectionid)->getSendAIO());	// Wait until aio send operation has finished to avoid crash when multiple send
}

void BaseNetworkModule::addRouteTo(uint8_t connectionid, uint8_t routetype, NetworkEntity target)
{
	m_Connections.at(connectionid)->addRoute(routetype, target);
}

void BaseNetworkModule::removeRouteFrom(uint8_t connectionid, uint8_t routetype, uint64_t id)
{
	m_Connections.at(connectionid)->removeRoute(routetype, id);
}

void BaseNetworkModule::onStart()
{
	for (std::unordered_map<uint8_t, std::unique_ptr<BaseNetworkConnection>>::iterator it = m_Connections.begin(); it != m_Connections.end(); ++it)
	{
		(*it).second->onStart();
	}
}
