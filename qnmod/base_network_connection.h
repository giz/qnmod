#pragma once

/**
* \class		BaseNetworkConnection
* \brief		Class representing a base network connection
* \author		Damien P.
* \date			2018
*/

#include <vector>
#include <unordered_map>
#include <memory>
#include <nng/nng.h>

class BaseMessage;

struct NetworkEntity {
	uint64_t id;
	nng_pipe pipe;
};

class BaseNetworkConnection
{
public:
	friend class BaseNetworkModule;

	/**
	* Mode enum
	* This is the connection type
	*/
	enum class Mode : uint8_t
	{
		kDial,	/**< DIAL MODE (CLIENT) */
		kListen	/**< LISTEN MODE (SERVER) */
	};

	/**
	* \brief Constructor
	*
	* Creates the network connection
	*/
	BaseNetworkConnection(BaseNetworkConnection::Mode mode, std::initializer_list<std::string> addresses);
	/**
	* \brief Destructor
	*
	* Destroy the network module
	*/
	virtual ~BaseNetworkConnection();


	/**
	* \brief Serialize a message
	*
	* Serialize a message before send it
	*
	* \param message : message you want to serialize
	* \return serialized message in bytes format
	*/
	virtual std::string serializeMessage(std::shared_ptr<BaseMessage> message) = 0;
	/**
	* \brief Unserialize a message
	*
	* Unserialize a message after received it
	*
	* \param pipe : the nng_pipe from where the msg came from
	* \param msg : the nng_msg received
	* \return unserialized msg in BaseMessage format
	*/
	virtual std::shared_ptr<BaseMessage> unserializeMessage(nng_pipe pipe, nng_msg* msg) = 0;
	/**
	* \brief onPostReceiveMessage
	*
	* This function is triggered just after a msg is received and unserialized with unserializeMessage
	* Use this function to dispatch the message to any module you want
	*
	* \param message : the message that was just received
	*/
	virtual void onPostReceiveMessage(std::shared_ptr<BaseMessage> message) = 0;
	/**
	* \brief onPreSendMessage
	*
	* This function is triggered just before a message is sent and just after it is serialized with serializeMessage
	* Use this function to set the good nng_pipe to the nng_msg
	*
	* \param routetype : type of the route to use
	* \param msg : the nng_msg
	* \param message : the serialized message
	*/
	virtual void onPreSendMessage(uint8_t routetype, nng_msg* msg, std::shared_ptr<BaseMessage> message) = 0;
	/**
	* \brief onPostSendMessage
	*
	* This function is triggered just after a msg is sent
	*/
	virtual void onPostSendMessage() = 0;
	/**
	* \brief onConnected
	*
	* This function is triggered just after a connection is done
	*
	* \param pipe : the nng_pipe that triggered this function
	* \param ev :
	*/
	virtual void onConnected(nng_pipe pipe, int ev) = 0;
	/**
	* \brief onDisconnected
	*
	* This function is triggered just after a connection is closed
	*
	* \param pipe : the nng_pipe that triggered this function
	* \param ev :
	*/
	virtual void onDisconnected(nng_pipe pipe, int ev) = 0;
	/**
	* \brief Get the mode
	*
	* Get the mode of the connection
	*
	* \return the mode of the connection
	*/
	const BaseNetworkConnection::Mode& getMode();
	/**
	* \brief Get addresses
	*
	* Get all addresses used by the connection
	*
	* \return the addresses
	*/
	const std::vector<std::string>& getAddresses();
	/**
	* \brief Get the socket
	*
	* Get the socket of the connection
	*
	* \return the nng_socket
	*/
	const nng_socket& getSocket();
	/**
	* \brief Get the receive AIO
	*
	* Get the AIO used to receive msg
	*
	* \return the nng_aio
	*/
	nng_aio* getRecvAIO();
	/**
	* \brief Get the send AIO
	*
	* Get the AIO used to send msg
	*
	* \return the nng_aio
	*/
	nng_aio* getSendAIO();
	/**
	* \brief Add a route
	*
	* Add a route
	*
	* \param type : type of the route
	* \param target : the route you want to add
	*/
	void addRoute(uint8_t type, NetworkEntity target);
	/**
	* \brief Remove a route
	*
	* Remove a route
	*
	* \param type : type of the route
	* \param id : id of the route you want to remove
	*/
	void removeRoute(uint8_t type, uint64_t id);
	/**
	* \brief Get a route
	*
	* Get a route from it's id
	*
	* \param type : type of the route
	* \param id : id of the route you want to get
	*/
	NetworkEntity& getRoute(uint8_t type, uint64_t id);
	/**
	* \brief Get a route
	*
	* Get a route from it's pipe
	*
	* \param type : type of the route
	* \param pipe : nng_pipe of the route you want to get
	*/
	NetworkEntity& getRoute(uint8_t type, nng_pipe pipe);
	/**
	* \brief Get the first route
	*
	* Get the first route of a specific type
	*
	* \param type : type of the route
	*/
	NetworkEntity& getFirstRoute(uint8_t type);

protected:
	std::unordered_map<uint8_t, std::unordered_map<uint64_t, NetworkEntity>> m_Routes;	/**< Map of maps of routes */

private:
	/**
	* \brief OnStart
	*
	* This function is triggered just before the loop is launched by the start function.
	*/
	void onStart();
	/**
	* \brief onReceiveMessage
	*
	* Callback used by nng
	*
	* \param arg : this BaseNetworkConnection
	*/
	static void onReceiveMessage(void* arg);
	/**
	* \brief onSendMessage
	*
	* Callback used by nng
	*
	* \param arg : this BaseNetworkConnection
	*/
	static void onSendMessage(void* arg);
	/**
	* \brief onConnected
	*
	* Callback used by nng
	*
	* \param pipe : the nng_pipe that trigger this function
	* \param ev :
	* \param arg : this BaseNetworkConnection
	*/
	static void onConnected(nng_pipe pipe, int ev, void* arg);
	/**
	* \brief onDisconnected
	*
	* Callback used by nng
	*
	* \param pipe : the nng_pipe that trigger this function
	* \param ev :
	* \param arg : this BaseNetworkConnection
	*/
	static void onDisconnected(nng_pipe pipe, int ev, void* arg);

	BaseNetworkConnection::Mode m_Mode;		/**< Mode of the connection */
	std::vector<std::string> m_Addresses;	/**< Vector of addresses */
	nng_socket m_Socket;					/**< Socket */
	nng_aio* m_RecvAIO;						/**< Receive AIO */
	nng_aio* m_SendAIO;						/**< Send AIO */
};
