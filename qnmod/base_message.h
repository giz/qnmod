#pragma once

/**
* \class		BaseMessage
* \brief		Class representing a base message
* \author		Damien P.
* \date			2018
*/

#include <cstdint>

class BaseMessage
{
public:
	/**
	* \brief Constructor
	*
	* Creates the message
	*/
	BaseMessage() {};
	/**
	* \brief Destructor
	*
	* Destroy the message
	*/
	virtual ~BaseMessage() {};

	/**
	* \brief Get the code
	*
	* Get the code of the message
	*
	* \return the code of the message
	*/
	virtual uint32_t getCode() = 0;
};