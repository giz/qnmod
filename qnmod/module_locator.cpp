#include "module_locator.h"
#include "base_module.h"



ModuleLocator::ModuleLocator()
{
}


ModuleLocator::~ModuleLocator()
{
}

void ModuleLocator::startAll()
{
	for (auto it = m_Modules.begin(); it != m_Modules.end(); ++it)
	{
		if (std::next(it) == m_Modules.end())
		{
			(*it).second->start(true);	// Start last module in main thread
		}
		else
		{
			(*it).second->start(false);	// Start all other modules in separate threads
		}
		
	}
}

void ModuleLocator::stopAll()
{
	for (auto it = m_Modules.begin(); it != m_Modules.end(); ++it)
	{
		if (std::next(it) == m_Modules.end())
		{
			(*it).second->stop(true);	// Stop main thread module
		}
		else
		{
			(*it).second->stop(false);	// Stop all threaded modules
		}
	}
}

void ModuleLocator::cleanUp()
{
	m_Modules.clear();
}

std::unordered_map<std::size_t, std::unique_ptr<BaseModule>> ModuleLocator::m_Modules;

