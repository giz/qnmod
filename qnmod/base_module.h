#pragma once

/**
* \class		BaseModule
* \brief		Class representing a base module
* \author		Damien P.
* \date			2018
*/

#include <thread>
#include <folly/io/async/EventBase.h>
#include <folly/fibers/FiberManagerMap.h>
#include "module_locator.h"

class BaseModule
{
public:
	friend void ModuleLocator::startAll();
	friend void ModuleLocator::stopAll();
	/**
	* \brief Constructor
	*
	* Creates the module
	*
	* \param hasupdate : set it if the module implements the update method
	* \param tickrate : tick rate of the update method
	* \param boundcore : the core on which the module is allowed to run (0 = every cores)
	*/
	BaseModule(bool hasupdate = false, double tickrate = 0.0, uint8_t boundcore = 0);
	/**
	* \brief Destructor
	*
	* Destroy the module
	*/
	virtual ~BaseModule() = default;

	/**
	* \brief Get eventbase
	*
	* Get the EventBase used by the module
	*
	* \return the EventBase used by the module
	*/
	folly::EventBase& getEventBase();

protected:
	/**
	* \brief Start a blocking task
	*
	* This function has to be used only if you know that you are going to block the module with a Future.then() for example.
	* Internally it launches the task in a fiber. The module can therefore proceed the next tasks in the queue.
	*
	* \param function : the function containing a blocking line
	*/
	void startBlockingTask(std::function<void()> function);

private:
	/**
	* \brief Bind the module to a core
	* 
	* Bind the module to a specific cpu core enabling a fine management of your module
	*  
	* \param handle : thread handle
	*/
	void bindToCore(std::thread::native_handle_type handle);
	/**
	* \brief Loop function
	* 
	* The loop function of the module
	*/
	void loop();
	/**
	* \brief Start the module
	*
	* Start the module in the main thread or in a separate thread
	*
	* \param mainthread : if true, start the module in the main thread, otherwise start the module in a separate thread
	*/
	void start(bool mainthread);
	/**
	* \brief Stop the module
	*
	* Stop the loop of the module
	*
	* \param mainthread : if true, stop the module in the main thread, otherwise stop the threaded module
	*/
	void stop(bool mainthread);
	/**
	* \brief OnStart
	*
	* This function is triggered just before the loop is launched by the start function.
	* If you want to do something before the start, you have to override this method.
	*/
	virtual void onStart();
	/**
	* \brief OnStop
	*
	* This function is triggered just after the loop is terminated by the stop function.
	* If you want to do something after the stop, you have to override this method.
	*/
	virtual void onStop();
	/**
	* \brief Update
	*
	* This function is triggered every m_TickRate.
	* If you want an update for your module, you have to override this method.
	*/
	virtual void update();

protected:
	folly::EventBase m_EventBase;	/**< EventBase used by the module */
	folly::fibers::FiberManager& m_FiberManager;	/**< FiberManager used by the module */
	std::thread m_Thread;	/**< Thread of the module */
	bool m_IsRunning;	/**< Tell if the module is running or not */
	bool m_HasUpdate;	/**< Tell if the module implements the update method */
	double m_TickRate;	/**< Tick rate of the update method */
	uint8_t m_BoundCore;	/**< The core on which the module is allowed to run */
};

