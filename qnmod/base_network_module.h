#pragma once

/**
* \class		BaseNetworkModule
* \brief		Class representing a base network module
* \author		Damien P.
* \date			2018
*/

#include "base_module.h"
#include "base_network_connection.h"

class BaseMessage;

class BaseNetworkModule : public BaseModule
{
public:
	/**
	* \brief Constructor
	*
	* Creates the network module
	*/
	BaseNetworkModule();
	/**
	* \brief Destructor
	*
	* Destroy the network module
	*/
	virtual ~BaseNetworkModule();


	/**
	* \brief Create a connection
	*
	* Create a connection
	*
	* \param connectionid : id of the connection you want to create
	* \param mode : connection mode (Client or Server)
	* \param addresses : list of addresses to use (dial or listen according to the connection mode)
	*/
	template<typename T>
	void createConnection(uint8_t connectionid, BaseNetworkConnection::Mode mode, std::initializer_list<std::string> addresses)
	{
		m_Connections.insert(std::make_pair(connectionid, std::make_unique<T>(mode, addresses)));
	};
	/**
	* \brief Send a message
	*
	* Send a message through the selected connection
	*
	* \param connectionid : id of the connection you want to use
	* \param routetype : type of the route you want to use
	* \param message : message you want to send
	*/
	void sendMessageTo(uint8_t connectionid, uint8_t routetype, std::shared_ptr<BaseMessage> message);
	/**
	* \brief Add a route
	*
	* Add a route to a connection
	*
	* \param connectionid : id of the connection
	* \param routetype : type of the route
	* \param target : route you want to add
	*/
	void addRouteTo(uint8_t connectionid, uint8_t routetype, NetworkEntity target);
	/**
	* \brief Remove a route
	*
	* Remove a route from a connection
	*
	* \param connectionid : id of the connection
	* \param routetype : type of the route
	* \param id : id of the route you want to remove
	*/
	void removeRouteFrom(uint8_t connectionid, uint8_t routetype, uint64_t id);

protected:
	std::unordered_map<uint8_t, std::unique_ptr<BaseNetworkConnection>> m_Connections;	/**< Map of connections */

private:
	/**
	* \brief OnStart
	*
	* This function is triggered just before the loop is launched by the start function.
	* If you want to do something before the start, you have to override this method.
	*/
	virtual void onStart();
};
