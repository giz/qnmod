#pragma once

/**
* \class		ModuleLocator
* \brief		Class representing a module locator
* \author		Damien P.
* \date			2018
*/

#include <unordered_map>
#include <memory>

class BaseModule;

class ModuleLocator
{
public:
	/**
	* \brief Constructor
	*
	* Creates the module locator
	*/
	ModuleLocator();
	/**
	* \brief Destructor
	*
	* Destroy the module locator
	*/
	~ModuleLocator();


	/**
	* \brief Start all modules
	*
	* Start all modules hold by the module locator
	*/
	static void startAll();
	/**
	* \brief Stop all modules
	*
	* Stop all modules hold by the module locator
	*/
	static void stopAll();
	/**
	* \brief Create a module
	*
	* Create a module and register it to the module locator
	*
	* \param args : arguments of the module constructor you want to create and register
	*/
	template <typename T, typename... Args>
	static void createModule(Args&&... args)
	{
		m_Modules.insert(std::make_pair(typeid(T).hash_code(), std::make_unique<T>(args...)));
	};
	/**
	* \brief Get a module
	*
	* Retrieve a module in the module locator
	*
	* \return the reference of the module you want or throw a std::out_of_range exception if the module is not found
	*/
	template<typename T>
	static T& getModule()
	{
		try
		{
			return dynamic_cast<T&>(*m_Modules.at(typeid(T).hash_code()));
		}
		catch (const std::out_of_range&)
		{
			throw;
		}
	};
	/**
	* \brief Enqueue task in another module
	*
	* This function is used to communicate between modules
	*
	* \param function : the task function
	* \param args... : arguments of the task function
	*/
	template<typename T, typename Fn, typename... Args>
	static void enqueueTaskInModule(Fn function, Args... args)
	{
		getModule<T>().getEventBase().runInEventBaseThread(std::bind(function, &getModule<T>(), args...));
	};
	/**
	* \brief Clean up
	*
	* Clean up the module locator freeing all modules in it. Use it before application shutdown.
	*/
	static void cleanUp();

private:
	static std::unordered_map<std::size_t, std::unique_ptr<BaseModule>> m_Modules;	/**< Map of modules */
};

