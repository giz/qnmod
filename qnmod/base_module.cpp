#include "base_module.h"



BaseModule::BaseModule(bool hasupdate, double tickrate, uint8_t boundcore) : m_FiberManager(folly::fibers::getFiberManager(m_EventBase)), m_IsRunning(false), m_HasUpdate(hasupdate), m_TickRate(tickrate), m_BoundCore(boundcore)
{
}

folly::EventBase& BaseModule::getEventBase()
{
	return m_EventBase;
}

void BaseModule::startBlockingTask(std::function<void()> function)
{
	m_FiberManager.addTask(std::move(function));
}

void BaseModule::bindToCore(std::thread::native_handle_type handle)
{
	if (m_BoundCore != 0)
	{
#ifdef _WIN32
		SetThreadAffinityMask(handle, 1i64 << m_BoundCore);
#elif linux
		cpu_set_t cpuset;
		CPU_ZERO(&cpuset);
		CPU_SET(m_BoundCore, &cpuset);
		pthread_setaffinity_np(handle, sizeof(cpu_set_t), &cpuset);
#endif			
	}
}

void BaseModule::loop()
{
	if (m_HasUpdate)
	{
		auto previous = std::chrono::high_resolution_clock::now();
		double lag = 0.0;

		while (m_IsRunning)
		{
			auto current = std::chrono::high_resolution_clock::now();
			auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(current - previous).count();
			previous = current;
			lag += elapsed;

			m_EventBase.loopOnce(EVLOOP_NONBLOCK);

			while (lag >= m_TickRate)
			{
				update();
				lag -= m_TickRate;
			}	
		}
	}
	else
	{
		m_EventBase.loopForever();
	}
}

void BaseModule::start(bool mainthread)
{
	m_IsRunning = true;
	if (mainthread)
	{
		// Bind the module to a physical core
#ifdef _WIN32
		bindToCore(GetCurrentThread());
#elif linux
		bindToCore(pthread_self());
#endif
		onStart();
		// Start event loop in main thread
		loop();
	}
	else
	{
		onStart();
		// Start event loop in a thread
		m_Thread = std::thread([&] {
			loop();
		});

		// Bind the module to a physical core
		bindToCore(m_Thread.native_handle());
	}
}

void BaseModule::stop(bool mainthread)
{
	m_IsRunning = false;
	m_EventBase.terminateLoopSoon();
	onStop();
	if (!mainthread)
	{
		m_Thread.join();
	}
}

void BaseModule::onStart()
{

}

void BaseModule::onStop()
{

}

void BaseModule::update()
{

}
