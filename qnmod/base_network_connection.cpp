#include "base_network_connection.h"
#include <nng/protocol/pair1/pair.h>

BaseNetworkConnection::BaseNetworkConnection(BaseNetworkConnection::Mode mode, std::initializer_list<std::string> addresses) : m_Mode(mode), m_Addresses(addresses), m_Socket(NNG_SOCKET_INITIALIZER)
{
	nng_pair1_open(&m_Socket);
	nng_aio_alloc(&m_RecvAIO, onReceiveMessage, this);
	nng_aio_alloc(&m_SendAIO, onSendMessage, this);
	nng_pipe_notify(m_Socket, NNG_PIPE_EV_ADD_POST, onConnected, this);
	nng_pipe_notify(m_Socket, NNG_PIPE_EV_REM_POST, onDisconnected, this);
	nng_setopt_bool(m_Socket, NNG_OPT_PAIR1_POLY, true);
}

BaseNetworkConnection::~BaseNetworkConnection()
{
	nng_aio_free(m_RecvAIO);
	nng_aio_free(m_SendAIO);
	nng_pipe_notify(m_Socket, NNG_PIPE_EV_REM_POST, NULL, NULL);	// Unregister this event before closing socket
	nng_close(m_Socket);
}

const BaseNetworkConnection::Mode& BaseNetworkConnection::getMode()
{
	return m_Mode;
}

const std::vector<std::string>& BaseNetworkConnection::getAddresses()
{
	return m_Addresses;
}

const nng_socket& BaseNetworkConnection::getSocket()
{
	return m_Socket;
}

nng_aio* BaseNetworkConnection::getRecvAIO()
{
	return m_RecvAIO;
}

nng_aio* BaseNetworkConnection::getSendAIO()
{
	return m_SendAIO;
}

void BaseNetworkConnection::addRoute(uint8_t type, NetworkEntity target)
{
	m_Routes[type].insert(std::make_pair(target.id, target));
}

void BaseNetworkConnection::removeRoute(uint8_t type, uint64_t id)
{
	m_Routes[type].erase(id);
}

NetworkEntity& BaseNetworkConnection::getRoute(uint8_t type, uint64_t id)
{
	return m_Routes[type].at(id);
}

NetworkEntity& BaseNetworkConnection::getRoute(uint8_t type, nng_pipe pipe)
{
	for (std::unordered_map< uint64_t, NetworkEntity>::iterator it = m_Routes[type].begin(); it != m_Routes[type].end(); ++it)
	{
		if (nng_pipe_id((*it).second.pipe) == nng_pipe_id(pipe))
		{
			return (*it).second;
		}
	}

	throw std::out_of_range("Pipe not found in route map");
}

NetworkEntity& BaseNetworkConnection::getFirstRoute(uint8_t type)
{
	return m_Routes[type].begin()->second;
}

void BaseNetworkConnection::onStart()
{
	if (m_Mode == BaseNetworkConnection::Mode::kDial)
	{
		for (std::vector<std::string>::const_iterator it = m_Addresses.begin(); it != m_Addresses.end(); ++it)
		{
			nng_dial(m_Socket, std::string("tcp://" + (*it)).c_str(), NULL, 0);
		}
	}
	else if (m_Mode == BaseNetworkConnection::Mode::kListen)
	{
		for (std::vector<std::string>::const_iterator it = m_Addresses.begin(); it != m_Addresses.end(); ++it)
		{
			nng_listen(m_Socket, std::string("tcp://" + (*it)).c_str(), NULL, 0);
		}
	}

	// Call recv to launch the recv loop
	nng_recv_aio(m_Socket, m_RecvAIO);
}

void BaseNetworkConnection::onReceiveMessage(void* arg)
{
	BaseNetworkConnection* conn = static_cast<BaseNetworkConnection*>(arg);

	int res = nng_aio_result(conn->getRecvAIO());	// If result == 0, operation is successful, we can proceed msg
	if (res == 0)
	{
		nng_msg* msg;
		msg = nng_aio_get_msg(conn->getRecvAIO());
		nng_pipe pipe = nng_msg_get_pipe(msg);

		std::shared_ptr<BaseMessage> message = conn->unserializeMessage(pipe, msg);
		conn->onPostReceiveMessage(message);
		
		nng_msg_free(msg);

		// Relaunch the recv loop
		nng_recv_aio(conn->getSocket(), conn->getRecvAIO());
	}
}

void BaseNetworkConnection::onSendMessage(void* arg)
{
	BaseNetworkConnection* conn = static_cast<BaseNetworkConnection*>(arg);

	const int res = nng_aio_result(conn->getSendAIO());
	if (res == 0)
	{
		conn->onPostSendMessage();
	}
	else
	{
		nng_msg* msg = nng_aio_get_msg(conn->getSendAIO());
		nng_msg_free(msg);
	}
}

void BaseNetworkConnection::onConnected(nng_pipe pipe, int ev, void* arg)
{
	BaseNetworkConnection* conn = static_cast<BaseNetworkConnection*>(arg);
	conn->onConnected(pipe, ev);
}

void BaseNetworkConnection::onDisconnected(nng_pipe pipe, int ev, void* arg)
{
	BaseNetworkConnection* conn = static_cast<BaseNetworkConnection*>(arg);
	conn->onDisconnected(pipe, ev);
}
